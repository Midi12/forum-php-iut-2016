<?php
	if(!isset($_SESSION))
		session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Forum AS 2016</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" type="text/css" href="css/register.css">
	</head>
	<body>
		<?php 
			require_once("header.php");
		?>
			<form id="formreg" name="formreg" method="POST" action="newuser.php">
				<input id="usernamebox" name="usernamebox" type="reg" placeholder="Username" pattern="[0-9A-Za-z\-]{2,}" required>
				<div class="inputvalidation"></div><br>
				<input id="emailbox" name="emailbox" type="email" placeholder="Mail" pattern="[0-9a-z-_+.]+@[0-9a-z-_+]+.[a-z]{2,4}" required>
				<div class="inputvalidation"></div><br>
				<input id="passwordbox" name="passwordbox" type="password" placeholder="Password" pattern="[0-9A-Za-z\-]{5,}" required>
				<div class="inputvalidation"></div><br>
				<input id="submitlogin" name="submitreg" type="submit" value="Register">
			</form>
		<?php 
			require_once("footer.php");
		?>
	</body>
</html>
