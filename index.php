<?php
	if(!isset($_SESSION))
		session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Forum AS 2016</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
	<body>
		<?php
			if (!isset($_COOKIE['disp']))
			{
				setcookie('disp', 5, time() + (3600 * 24 * 365));
			}
			if (!isset($_GET['disp']))
			{
				header('Location: index.php?disp='.$_COOKIE['disp'].(isset($_GET['tag']) ? "&tag=".$_GET['tag'] : ""));
			}
			require_once("header.php");
		?>
		<?php
			echo '<nav id="tags">';
			if (session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['username']))
			{
				echo '<a class="button" href="newthread.php">Start a new thread</a>';
			}
			require_once("db_connection.php");
			$bdd = connect_db();
			$q = $bdd->query("SELECT DISTINCT idcat, nomcat FROM categorie NATURAL JOIN appartenir NATURAL JOIN sujet ORDER BY datemajsujet DESC LIMIT 10");
			while ($d = $q->fetch())
			{
				echo '<a href="index.php?tag='.$d['idcat'].'" class="tag">#'.$d['nomcat'].'</a>';
			}
			echo '</nav>';
			$i = 0;
			$req = 'SELECT * FROM sujet NATURAL JOIN membre';
			if (isset($_GET['tag']))
			{
				$req = $req.' NATURAL JOIN appartenir where idCat = '.$_GET['tag'];
			}
			$req = $req.' ORDER BY datemajsujet DESC;';
			$q = $bdd->query($req);?>
			<div id="divcontent"><?php
			if (isset($_GET['disp']))
				$nbpost = $_GET['disp'];
			else
				$nbpost = 10;
			if (isset($_GET['page']))
				$i = $_GET['page'] * $nbpost;
			else
				$i = 0;
			$j = 0;
			while ($j < $i)
			{
				$d = $q->fetch();
				$j++;
			}
			if ($q->rowCount() - $i <= 0)
				header('Location: '.$_SERVER['HTTP_REFERER']);
			$i = 0;
			while ($d = $q->fetch() and $i < $nbpost)
			{?>
				<div class="subject dynorder">
					<a href="thread.php?id=<?php echo $d['idsujet'] ?>" class="title"><?php echo $d['titresujet']; ?></a><br>
				<span class="note">Submission Date: <?php echo $d['datesujet']; ?></span>
				<?php
					if (session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['rank']))
					{
						if ($_SESSION['rank'] == 'ADMIN')
						{
							echo '<a class="button" href="#" onclick="confirmDeleteThread('.$d['idsujet'].')">Delete thread</a>';
						}
					}
				?><br><span class="note">Updated on: <?php echo $d['datemajsujet']; ?></span><br>
				by <a href="member.php?username=<?php echo $d['pseudomembre']; ?>" class="member"><?php echo $d['pseudomembre']; ?></a><br>
				
				<?php require_once('utils.php'); displaytag($d['idsujet']); ?>
				</div><?php
				$i++;
			}
			?></div><?php
		?>
		<?php 
			echo "<div id=\"divfooter\">";
				if (isset($_GET['disp']))
				{
					$url = 'index.php?';
					if (isset($_GET['page']))
						$page = $_GET['page'];
					else
						$page = 0;
					if (isset($_GET['disp']))
						$url = $url.'disp='.$_GET['disp'].'&';
					echo '<a href="'.$url.'page='.((string)($page + ($page <= 0 ? 0 : -1))).'" class="navlink prev">prev</a>';
					echo '<div id="divfootercenter"></div>';
					echo '<a href="'.$url.'page='.((string)$page + 1).'" class="navlink next">next</a>';
				}
			echo "</div>";
			require_once("footer.php");
		?>
		<script src="scripts/jquery.js"></script>
		<script src="scripts/scripts.js"></script>
	</body>
</html>
