<?php
	if(!isset($_SESSION))
		session_start();
?>
<?php
	session_unset();
	session_destroy();
	header('Location: '.$_SERVER['HTTP_REFERER']);
?>
