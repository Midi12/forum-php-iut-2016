<?php
	if(!isset($_SESSION))
		session_start();
?>

<?php
	//session active check
	if(session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['username']))
	{
		//sanity checks
		if (!isset($_POST['content']) || !isset($_POST['title']))
		{
			header("location:index.php");
		}
		else
		{
			require_once("db_connection.php");
			$bdd = connect_db();
			
			//get member id
			$prequery0 = "SELECT idmembre FROM membre WHERE pseudomembre = '" . $_SESSION['username'] . "'";
			
			if ($q0 = $bdd->query($prequery0))
			{
				$idmembre = $q0->fetch()[0];
			}
			else
			{
				die("erreur requete :" . $prequery0 . "\n");
			}
			
			//parsing tags 
			$rawtags = $_POST['tags'];
			$tagarray = explode(';', $rawtags);
			
			foreach ($tagarray as $tag)
			{
				$tagquery = "SELECT COUNT(idcat) FROM categorie WHERE nomcat = '" . $tag . "'";
				if ($qtag = $bdd->query($tagquery))
				{
					if (intval($qtag->fetch()[0]) == 0 and $tag != "")
					{
						$tagqueryins = "INSERT INTO categorie (nomcat) VALUES ('" . $tag . "')";
						if (!($qtagins = $bdd->query($tagqueryins)))
						{
							die("erreur query: " . $tagqueryins);
						}
					}
				}
				else
				{
					die("erreur query: " . $tagquery);
				}
			}
			
			//insert new thread
			$query_thread = "INSERT INTO sujet (titresujet, idmembre, datesujet, datemajsujet) VALUES ('" . $_POST['title'] . "', " . $idmembre . ", NOW(), NOW())";
			
			if ($qt = $bdd->query($query_thread))
			{
				$query_gettid = "SELECT idsujet FROM sujet WHERE titresujet = '" . $_POST['title'] . "' ORDER BY datesujet DESC";
				
				if ($qg = $bdd->query($query_gettid))
				{
					$tid = intval($qg->fetch()[0]);
					
					//insert message of the new created thread
					$query_msg = "INSERT INTO message (idsujet, idmessage, contenumessage, idmembre, datemessage) VALUES (" . $tid . ", 1, \"" . $_POST['content'] . "\", " . $idmembre . ", NOW())";
					if(!($qm = $bdd->query($query_msg)))
					{
						die("erreur requete : " . $query_msg);
					}
				}
				else
				{
					die("erreur requete : " . $query_gettid);
				}
			}
			else
			{
				die("erreur requete: " . $query_thread);
			}
			
			foreach ($tagarray as $tag)
			{
				if ($tag != "")
				{
					$query_tagid = "SELECT idcat FROM categorie WHERE nomcat = '" . $tag . "' ";
					
					if ($qti = $bdd->query($query_tagid))
					{
						$query_link = "INSERT INTO appartenir (idcat, idsujet) VALUES (" . $qti->fetch()[0] . ", " . $tid . ")";
						if (!($ql = $bdd->query($query_link)))
						{
							die ("erreur query: " . $query_link);
						}
					}
				}
			}
			
			header("location:thread.php?id=" . $tid);
		}
	}
	else
	{
		header("location:index.php");
	}
?>
