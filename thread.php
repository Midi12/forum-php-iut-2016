<?php
	if(!isset($_SESSION))
		session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>FORUM IUT PHP 2016</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
	<body>
		<!-- header -->
		<?php 
			require_once("header.php");
		?>
		
		<!-- insert nav here -->
		
		<!-- header -->
		
		<!-- content -->
		<!-- CONTENT INSIDE DIVCONTENT WILL BE DYNAMICALY GENERATED -->
		
		<div id="divcontent">
			<?php
				if(!isset($_GET['id']))
				{
					echo "<span>Unknown thread id ...</span>";
				}
				else
				{
					require_once("db_connection.php");
					$bdd = connect_db();
					
					if($q = $bdd->query("SELECT * FROM message WHERE idsujet = " . $_GET['id'] . " ORDER BY idmessage ASC"))
					{			
						while($msg = $q->fetch())
						{
							$q1 = $bdd->query("SELECT pseudomembre FROM membre WHERE idmembre = " . $msg['idmembre']);
							
							if($q1 == NULL)
								$user = "erreur q1";
							else
								$user = $q1->fetch()[0];
							
							if ($msg['idmessage'] == 1)
							{
								$q0 = $bdd->query("SELECT titresujet FROM sujet WHERE idsujet = " . $_GET['id']);
						
								if($q0 == NULL)
									$title = "erreur q0";
								else
									$title = $q0->fetch()[0];
								
								echo "<div class=\"subject\">";
									echo "<a href=\"#\" class=\"title\">" . $title . "</a>";
									
									if(session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['username']))
									{
										if ($_SESSION['username'] == $user)
										{
											echo "<span><a class=\"button\" href=\"#\" id=\"" . $msg['idmessage'] . "\" onclick=\"ShowEditForm(" . $msg['idmessage'] . ")\" >Edit</a></span>";
										}
									}
									
									echo "<br>";
									echo "<span class=\"note\">Updated on: " . $msg['datemessage'] . "</span><br>";
									echo "by <a href=\"member.php?username=" . $user . "\" class=\"member\">" . $user . "</a><br>";
									require_once('utils.php');
									displaytag($_GET['id']);
									echo "<p class=\"message\">" . $msg['contenumessage'] . "<p>";
								echo "</div>";
							}
							else
							{	
								echo "<div class=\"answer dynorder\">";
									echo "<span class=\"note\">Updated on: " . $msg['datemessage'] . "</span>";
									
									if(session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['username']))
									{	
										if ($_SESSION['username'] == $user)
										{
											echo "<span><a class=\"button\" href=\"#\" id=\"" . $msg['idmessage'] . "\" onclick=\"ShowEditForm(" . $msg['idmessage'] . ")\" >Edit</a></span>";
										}
										if ($_SESSION['rank'] == 'ADMIN' || $_SESSION['username'] == $user)
										{
											echo "<span><a class=\"button\" href=\"#\" onclick=\"confirmDeletePost(" . $msg['idmessage'] . ")\" >Delete post</a></span>";
										}
									}
									
									echo "<br>";
									echo "by <a href=\"member.php?username=" . $user . "\" class=\"member\">". $user . "</a><br>";
									echo "<p class=\"message\">" . $msg['contenumessage'] . "<p>";
								echo "</div>";
							}
						}
						
						if(session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['username']))
						{
							echo "<div class=\"divreply ensurelastorder\">";
								echo "<input type=\"button\" value=\"Reply\" onclick=\"ShowReplyForm()\">";
							echo "</div>";
						}
					}
					else
					{
						echo "Erreur requete ...";
					}
				}
			?>
		</div>
		<!-- content -->
		
		<!-- footer -->
		<?php 
			require_once("footer.php");
		?>
		<!-- footer -->
		
		<script src="scripts/jquery.js"></script>
		<script src="scripts/scripts.js"></script>
	</body>
</html>
