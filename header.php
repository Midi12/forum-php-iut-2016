<?php
	if (!isset($_SESSION))
		session_start();
?>

<?php
	echo '<div id="divheader">';
		echo '<div id="divbanner">';
			echo '<a href="index.php"><img src="pictures/snoop.gif" alt="Snoop Dawg" class="banner"><img src="pictures/banner.png" alt="Snoop Dawg" class="banner"></a>';
		echo '</div>';
		echo '<div id="divlogin">';
		
		if (!isset($_SESSION['username']))
		{
			echo '<form id="formlogin" name="formlogin" method="POST" action="login.php">';
				echo "<span class='mininote'>Not a member? <a href='register.php'>Register!</a></span><br/>";
				echo '<input id="usernamebox" name="usernamebox" type="reg" placeholder="username"><br>';
				echo '<input id="passwordbox" name="passwordbox" type="password" placeholder="••••••••••••"><br>';
				echo '<input id="rememberbox" name="rememberbox" type="checkbox"><label for="rememberbox">Remember me</label>';
				echo '<input id="submitlogin" name="submitlogin" type="submit" value="Login">';
			echo '</form>';
		}
		else
		{
			echo '<span class="note">Welcome Home, '.$_SESSION['username'].'!</span><br>';
			echo '<span class="note"><a href="usercp.php?username=' . $_SESSION['username'] . '">User Control Panel</a></span>';
			echo '<form id="formlogoff" name="formlogin" method="GET" action="logoff.php">';
				echo '<input id="submitlogin" name="submitlogin" type="submit" value="Logoff">';
			echo '</form>';
		}
		echo '</div>';
	echo '</div>';
?>
