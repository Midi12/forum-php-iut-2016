CREATE DATABASE IF NOT EXISTS `MEMBRES` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `MEMBRES`;

CREATE TABLE `MESSAGE` (
  `idmessage` VARCHAR(42),
  `contenumessage` VARCHAR(42),
  `idmembre` VARCHAR(42),
  PRIMARY KEY (`idmessage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MEMBRE` (
  `idmembre` VARCHAR(42),
  `pseudomembre` VARCHAR(42),
  `mailmembre` VARCHAR(42),
  `passwordmembre` VARCHAR(42),
  `rangmembre` VARCHAR(42),
  PRIMARY KEY (`idmembre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `SUJET` (
  `idsujet` VARCHAR(42),
  `titresujet` VARCHAR(42),
  `idmembre` VARCHAR(42),
  `idmessage` VARCHAR(42),
  PRIMARY KEY (`idsujet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CATEGORIE` (
  `idcat` VARCHAR(42),
  `nomcat` VARCHAR(42),
  PRIMARY KEY (`idcat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `APPARTENIR` (
  `idcat` VARCHAR(42),
  `idsujet` VARCHAR(42),
  PRIMARY KEY (`idcat`, `idsujet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `MESSAGE` ADD FOREIGN KEY (`idmembre`) REFERENCES `MEMBRE` (`idmembre`);
ALTER TABLE `SUJET` ADD FOREIGN KEY (`idmessage`) REFERENCES `MESSAGE` (`idmessage`);
ALTER TABLE `SUJET` ADD FOREIGN KEY (`idmembre`) REFERENCES `MEMBRE` (`idmembre`);
ALTER TABLE `APPARTENIR` ADD FOREIGN KEY (`idsujet`) REFERENCES `SUJET` (`idsujet`);
ALTER TABLE `APPARTENIR` ADD FOREIGN KEY (`idcat`) REFERENCES `CATEGORIE` (`idcat`);