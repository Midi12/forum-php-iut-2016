#!/usr/bin/env python
# encoding: utf-8
# Généré par Mocodo 2.0.17 le Thu, 17 Dec 2015 00:29:21

import time, codecs

import json

geo = json.loads(open('Membres_geo.json').read())
(width,height) = geo.pop('size')
for (name, l) in geo.iteritems(): globals()[name] = dict(l)
card_max_width = 25
card_max_height = 14
card_margin = 5
arrow_width = 12
arrow_half_height = 6
arrow_axis = 8


def card_pos(ex, ey, ew, eh, ax, ay, k):
    if ax != ex and abs(float(ay - ey) / (ax - ex)) < float(eh) / ew:
        (x0, x1) = (ex + cmp(ax, ex) * (ew + card_margin), ex + cmp(ax, ex) * (ew + card_margin + card_max_width))
        (y0, y1) = sorted([ey + (x0 - ex) * (ay - ey) / (ax - ex), ey + (x1 - ex) * (ay - ey) / (ax - ex)])
        return (min(x0, x1), (y0 + y1 - card_max_height + k * abs(y1 - y0 + card_max_height)) / 2 + cmp(k, 0) * card_margin)
    else:
        (y0, y1) = (ey + cmp(ay, ey) * (eh + card_margin), ey + cmp(ay, ey) * (eh + card_margin + card_max_height))
        (x0, x1) = sorted([ex + (y0 - ey) * (ax - ex) / (ay - ey), ex + (y1 - ey) * (ax - ex) / (ay - ey)])
        return ((x0 + x1 - card_max_width + k * abs(x1 - x0 + card_max_width)) / 2 + cmp(k, 0) * card_margin, min(y0, y1))


def line_arrow(x0, y0, x1, y1, t):
    (x, y) = (t * x0 + (1 - t) * x1, t * y0 + (1 - t) * y1)
    return arrow(x, y, x1 - x0, y0 - y1)


def curve_arrow(x0, y0, x1, y1, x2, y2, x3, y3, t):
    (cx, cy) = (3 * (x1 - x0), 3 * (y1 - y0))
    (bx, by) = (3 * (x2 - x1) - cx, 3 * (y2 - y1) - cy)
    (ax, ay) = (x3 - x0 - cx - bx, y3 - y0 - cy - by)
    t = 1 - t
    bezier = lambda t: (ax*t*t*t + bx*t*t + cx*t + x0, ay*t*t*t + by*t*t + cy*t + y0)
    (x, y) = bezier(t)
    u = 1.0
    while t < u:
        m = (u + t) / 2.0
        (xc, yc) = bezier(m)
        d = ((x - xc)**2 + (y - yc)**2)**0.5
        if abs(d - arrow_axis) < 0.01:
            break
        if d > arrow_axis:
            u = m
        else:
            t = m
    return arrow(x, y, xc - x, y - yc)


def upper_round_rect(x, y, w, h, r):
    return " ".join([unicode(x) for x in ["M", x + w - r, y, "a", r, r, 90, 0, 1, r, r, "V", y + h, "h", -w, "V", y + r, "a", r, r, 90, 0, 1, r, -r]])

def lower_round_rect(x, y, w, h, r):
    return " ".join([unicode(x) for x in ["M", x + w, y, "v", h - r, "a", r, r, 90, 0, 1, -r, r, "H", x + r, "a", r, r, 90, 0, 1, -r, -r, "V", y, "H", w]])

def arrow(x, y, a, b):
    c = (a * a + b * b)**0.5
    (cos, sin) = (a / c, b / c)
    return " ".join([unicode(x) for x in [ "M", x, y, "L", x + arrow_width * cos - arrow_half_height * sin, y - arrow_half_height * cos - arrow_width * sin, "L", x + arrow_axis * cos, y - arrow_axis * sin, "L", x + arrow_width * cos + arrow_half_height * sin, y + arrow_half_height * cos - arrow_width * sin, "Z"]])

def safe_print_for_PHP(s):
    try:
        print s
    except UnicodeEncodeError:
        print s.encode("utf8")


lines = '<?xml version="1.0" standalone="no"?>\n<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"\n"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">'
lines += '\n\n<svg width="%s" height="%s" view_box="0 0 %s %s"\nxmlns="http://www.w3.org/2000/svg"\nxmlns:link="http://www.w3.org/1999/xlink">' % (width,height,width,height)
lines += u'\\n\\n<desc>Généré par Mocodo 2.0.17 le %s</desc>' % time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())
lines += '\n\n<rect id="frame" x="0" y="0" width="%s" height="%s" fill="%s" stroke="none" stroke-width="0"/>' % (width,height,colors['background_color'] if colors['background_color'] else "none")

lines += u"""\n\n<!-- Association PUBLIER_SUJET -->"""
(x,y) = (cx[u"PUBLIER_SUJET"],cy[u"PUBLIER_SUJET"])
(ex,ey) = (cx[u"MEMBRE"],cy[u"MEMBRE"])
lines += u"""\n<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x0': ex, 'y0': ey, 'x1': x, 'y1': y, 'stroke_color': colors['leg_stroke_color']}
(tx,ty) = card_pos(ex,11.9+ey,58,56,x,11.9+y,k[u"PUBLIER_SUJET,MEMBRE"])
lines += u"""\n<text x="%(tx)s" y="%(ty)s" fill="%(text_color)s" font-family="Verdana" font-size="12">0,N</text>""" % {'tx': tx, 'ty': ty, 'text_color': colors['card_text_color']}
path = line_arrow(ex,ey,x,y,t[u"PUBLIER_SUJET,MEMBRE"])
lines += u"""\n<path d="%(path)s" fill="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'stroke_color': colors['leg_stroke_color']}
(ex,ey) = (cx[u"SUJET"],cy[u"SUJET"])
lines += u"""\n<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x0': ex, 'y0': ey, 'x1': x, 'y1': y, 'stroke_color': colors['leg_stroke_color']}
(tx,ty) = card_pos(ex,11.9+ey,34,32,x,11.9+y,k[u"PUBLIER_SUJET,SUJET"])
lines += u"""\n<text x="%(tx)s" y="%(ty)s" fill="%(text_color)s" font-family="Verdana" font-size="12">1,1</text>""" % {'tx': tx, 'ty': ty, 'text_color': colors['card_text_color']}
lines += u"""\n<g id="association-PUBLIER_SUJET">""" % {}
path = upper_round_rect(-56+x,-24+y,112,24,14)
lines += u"""\n	<path d="%(path)s" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'color': colors['association_cartouche_color'], 'stroke_color': colors['association_cartouche_color']}
path = lower_round_rect(-56+x,0+y,112,24,14)
lines += u"""\n	<path d="%(path)s" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'color': colors['association_color'], 'stroke_color': colors['association_color']}
lines += u"""\n	<rect x="%(x)s" y="%(y)s" width="112" height="48" fill="%(color)s" rx="14" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x': -56+x, 'y': -24+y, 'color': colors['transparent_color'], 'stroke_color': colors['association_stroke_color']}
lines += u"""\n	<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -56+x, 'y0': 0+y, 'x1': 56+x, 'y1': 0+y, 'stroke_color': colors['association_stroke_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">PUBLIER_SUJET</text>""" % {'x': -49+x, 'y': -7.1+y, 'text_color': colors['association_cartouche_text_color']}
lines += u"""\n</g>""" % {}

lines += u"""\n\n<!-- Association PUBLIER_MESSAGE -->"""
(x,y) = (cx[u"PUBLIER_MESSAGE"],cy[u"PUBLIER_MESSAGE"])
(ex,ey) = (cx[u"MEMBRE"],cy[u"MEMBRE"])
lines += u"""\n<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x0': ex, 'y0': ey, 'x1': x, 'y1': y, 'stroke_color': colors['leg_stroke_color']}
(tx,ty) = card_pos(ex,11.9+ey,58,56,x,11.9+y,k[u"PUBLIER_MESSAGE,MEMBRE"])
lines += u"""\n<text x="%(tx)s" y="%(ty)s" fill="%(text_color)s" font-family="Verdana" font-size="12">0,N</text>""" % {'tx': tx, 'ty': ty, 'text_color': colors['card_text_color']}
path = line_arrow(ex,ey,x,y,t[u"PUBLIER_MESSAGE,MEMBRE"])
lines += u"""\n<path d="%(path)s" fill="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'stroke_color': colors['leg_stroke_color']}
(ex,ey) = (cx[u"MESSAGE"],cy[u"MESSAGE"])
lines += u"""\n<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x0': ex, 'y0': ey, 'x1': x, 'y1': y, 'stroke_color': colors['leg_stroke_color']}
(tx,ty) = card_pos(ex,11.9+ey,55,32,x,11.9+y,k[u"PUBLIER_MESSAGE,MESSAGE"])
lines += u"""\n<text x="%(tx)s" y="%(ty)s" fill="%(text_color)s" font-family="Verdana" font-size="12">1,1</text>""" % {'tx': tx, 'ty': ty, 'text_color': colors['card_text_color']}
lines += u"""\n<g id="association-PUBLIER_MESSAGE">""" % {}
path = upper_round_rect(-67+x,-24+y,134,24,14)
lines += u"""\n	<path d="%(path)s" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'color': colors['association_cartouche_color'], 'stroke_color': colors['association_cartouche_color']}
path = lower_round_rect(-67+x,0+y,134,24,14)
lines += u"""\n	<path d="%(path)s" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'color': colors['association_color'], 'stroke_color': colors['association_color']}
lines += u"""\n	<rect x="%(x)s" y="%(y)s" width="134" height="48" fill="%(color)s" rx="14" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x': -67+x, 'y': -24+y, 'color': colors['transparent_color'], 'stroke_color': colors['association_stroke_color']}
lines += u"""\n	<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -67+x, 'y0': 0+y, 'x1': 67+x, 'y1': 0+y, 'stroke_color': colors['association_stroke_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">PUBLIER_MESSAGE</text>""" % {'x': -60+x, 'y': -7.1+y, 'text_color': colors['association_cartouche_text_color']}
lines += u"""\n</g>""" % {}

lines += u"""\n\n<!-- Association CONTENIR -->"""
(x,y) = (cx[u"CONTENIR"],cy[u"CONTENIR"])
(ex,ey) = (cx[u"MESSAGE"],cy[u"MESSAGE"])
lines += u"""\n<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x0': ex, 'y0': ey, 'x1': x, 'y1': y, 'stroke_color': colors['leg_stroke_color']}
(tx,ty) = card_pos(ex,11.9+ey,55,32,x,11.9+y,k[u"CONTENIR,MESSAGE"])
lines += u"""\n<text x="%(tx)s" y="%(ty)s" fill="%(text_color)s" font-family="Verdana" font-size="12">1,N</text>""" % {'tx': tx, 'ty': ty, 'text_color': colors['card_text_color']}
path = line_arrow(ex,ey,x,y,t[u"CONTENIR,MESSAGE"])
lines += u"""\n<path d="%(path)s" fill="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'stroke_color': colors['leg_stroke_color']}
(ex,ey) = (cx[u"SUJET"],cy[u"SUJET"])
lines += u"""\n<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x0': ex, 'y0': ey, 'x1': x, 'y1': y, 'stroke_color': colors['leg_stroke_color']}
(tx,ty) = card_pos(ex,11.9+ey,34,32,x,11.9+y,k[u"CONTENIR,SUJET"])
lines += u"""\n<text x="%(tx)s" y="%(ty)s" fill="%(text_color)s" font-family="Verdana" font-size="12">1,1</text>""" % {'tx': tx, 'ty': ty, 'text_color': colors['card_text_color']}
lines += u"""\n<g id="association-CONTENIR">""" % {}
path = upper_round_rect(-39+x,-24+y,78,24,14)
lines += u"""\n	<path d="%(path)s" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'color': colors['association_cartouche_color'], 'stroke_color': colors['association_cartouche_color']}
path = lower_round_rect(-39+x,0+y,78,24,14)
lines += u"""\n	<path d="%(path)s" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'color': colors['association_color'], 'stroke_color': colors['association_color']}
lines += u"""\n	<rect x="%(x)s" y="%(y)s" width="78" height="48" fill="%(color)s" rx="14" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x': -39+x, 'y': -24+y, 'color': colors['transparent_color'], 'stroke_color': colors['association_stroke_color']}
lines += u"""\n	<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -39+x, 'y0': 0+y, 'x1': 39+x, 'y1': 0+y, 'stroke_color': colors['association_stroke_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">CONTENIR</text>""" % {'x': -32+x, 'y': -7.1+y, 'text_color': colors['association_cartouche_text_color']}
lines += u"""\n</g>""" % {}

lines += u"""\n\n<!-- Association APPARTENIR -->"""
(x,y) = (cx[u"APPARTENIR"],cy[u"APPARTENIR"])
(ex,ey) = (cx[u"CATEGORIE"],cy[u"CATEGORIE"])
lines += u"""\n<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x0': ex, 'y0': ey, 'x1': x, 'y1': y, 'stroke_color': colors['leg_stroke_color']}
(tx,ty) = card_pos(ex,11.9+ey,41,32,x,11.9+y,k[u"APPARTENIR,CATEGORIE"])
lines += u"""\n<text x="%(tx)s" y="%(ty)s" fill="%(text_color)s" font-family="Verdana" font-size="12">1,N</text>""" % {'tx': tx, 'ty': ty, 'text_color': colors['card_text_color']}
(ex,ey) = (cx[u"SUJET"],cy[u"SUJET"])
lines += u"""\n<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x0': ex, 'y0': ey, 'x1': x, 'y1': y, 'stroke_color': colors['leg_stroke_color']}
(tx,ty) = card_pos(ex,11.9+ey,34,32,x,11.9+y,k[u"APPARTENIR,SUJET"])
lines += u"""\n<text x="%(tx)s" y="%(ty)s" fill="%(text_color)s" font-family="Verdana" font-size="12">0,N</text>""" % {'tx': tx, 'ty': ty, 'text_color': colors['card_text_color']}
lines += u"""\n<g id="association-APPARTENIR">""" % {}
path = upper_round_rect(-46+x,-24+y,92,24,14)
lines += u"""\n	<path d="%(path)s" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'color': colors['association_cartouche_color'], 'stroke_color': colors['association_cartouche_color']}
path = lower_round_rect(-46+x,0+y,92,24,14)
lines += u"""\n	<path d="%(path)s" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'path': path, 'color': colors['association_color'], 'stroke_color': colors['association_color']}
lines += u"""\n	<rect x="%(x)s" y="%(y)s" width="92" height="48" fill="%(color)s" rx="14" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x': -46+x, 'y': -24+y, 'color': colors['transparent_color'], 'stroke_color': colors['association_stroke_color']}
lines += u"""\n	<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -46+x, 'y0': 0+y, 'x1': 46+x, 'y1': 0+y, 'stroke_color': colors['association_stroke_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">APPARTENIR</text>""" % {'x': -39+x, 'y': -7.1+y, 'text_color': colors['association_cartouche_text_color']}
lines += u"""\n</g>""" % {}

lines += u"""\n\n<!-- Entity CATEGORIE -->"""
(x,y) = (cx[u"CATEGORIE"],cy[u"CATEGORIE"])
lines += u"""\n<g id="entity-CATEGORIE">""" % {}
lines += u"""\n	<g id="frame-CATEGORIE">""" % {}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="82" height="24" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'x': -41+x, 'y': -32+y, 'color': colors['entity_cartouche_color'], 'stroke_color': colors['entity_cartouche_color']}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="82" height="40" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'x': -41+x, 'y': -8+y, 'color': colors['entity_color'], 'stroke_color': colors['entity_color']}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="82" height="64" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x': -41+x, 'y': -32+y, 'color': colors['transparent_color'], 'stroke_color': colors['entity_stroke_color']}
lines += u"""\n		<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -41+x, 'y0': -8+y, 'x1': 41+x, 'y1': -8+y, 'stroke_color': colors['entity_stroke_color']}
lines += u"""\n	</g>""" % {}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">CATEGORIE</text>""" % {'x': -36+x, 'y': -15.1+y, 'text_color': colors['entity_cartouche_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">idCat</text>""" % {'x': -36+x, 'y': 8.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n	<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -36+x, 'y0': 11+y, 'x1': -5+x, 'y1': 11+y, 'stroke_color': colors['entity_attribute_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">NomCat</text>""" % {'x': -36+x, 'y': 24.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n</g>""" % {}

lines += u"""\n\n<!-- Entity MESSAGE -->"""
(x,y) = (cx[u"MESSAGE"],cy[u"MESSAGE"])
lines += u"""\n<g id="entity-MESSAGE">""" % {}
lines += u"""\n	<g id="frame-MESSAGE">""" % {}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="110" height="24" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'x': -55+x, 'y': -32+y, 'color': colors['entity_cartouche_color'], 'stroke_color': colors['entity_cartouche_color']}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="110" height="40" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'x': -55+x, 'y': -8+y, 'color': colors['entity_color'], 'stroke_color': colors['entity_color']}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="110" height="64" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x': -55+x, 'y': -32+y, 'color': colors['transparent_color'], 'stroke_color': colors['entity_stroke_color']}
lines += u"""\n		<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -55+x, 'y0': -8+y, 'x1': 55+x, 'y1': -8+y, 'stroke_color': colors['entity_stroke_color']}
lines += u"""\n	</g>""" % {}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">MESSAGE</text>""" % {'x': -30+x, 'y': -15.1+y, 'text_color': colors['entity_cartouche_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">idMessage</text>""" % {'x': -50+x, 'y': 8.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n	<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -50+x, 'y0': 11+y, 'x1': 12+x, 'y1': 11+y, 'stroke_color': colors['entity_attribute_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">contenuMessage</text>""" % {'x': -50+x, 'y': 24.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n</g>""" % {}

lines += u"""\n\n<!-- Entity SUJET -->"""
(x,y) = (cx[u"SUJET"],cy[u"SUJET"])
lines += u"""\n<g id="entity-SUJET">""" % {}
lines += u"""\n	<g id="frame-SUJET">""" % {}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="68" height="24" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'x': -34+x, 'y': -32+y, 'color': colors['entity_cartouche_color'], 'stroke_color': colors['entity_cartouche_color']}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="68" height="40" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'x': -34+x, 'y': -8+y, 'color': colors['entity_color'], 'stroke_color': colors['entity_color']}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="68" height="64" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x': -34+x, 'y': -32+y, 'color': colors['transparent_color'], 'stroke_color': colors['entity_stroke_color']}
lines += u"""\n		<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -34+x, 'y0': -8+y, 'x1': 34+x, 'y1': -8+y, 'stroke_color': colors['entity_stroke_color']}
lines += u"""\n	</g>""" % {}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">SUJET</text>""" % {'x': -19+x, 'y': -15.1+y, 'text_color': colors['entity_cartouche_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">idSujet</text>""" % {'x': -29+x, 'y': 8.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n	<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -29+x, 'y0': 11+y, 'x1': 14+x, 'y1': 11+y, 'stroke_color': colors['entity_attribute_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">titreSujet</text>""" % {'x': -29+x, 'y': 24.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n</g>""" % {}

lines += u"""\n\n<!-- Entity MEMBRE -->"""
(x,y) = (cx[u"MEMBRE"],cy[u"MEMBRE"])
lines += u"""\n<g id="entity-MEMBRE">""" % {}
lines += u"""\n	<g id="frame-MEMBRE">""" % {}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="116" height="24" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'x': -58+x, 'y': -56+y, 'color': colors['entity_cartouche_color'], 'stroke_color': colors['entity_cartouche_color']}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="116" height="88" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="0"/>""" % {'x': -58+x, 'y': -32+y, 'color': colors['entity_color'], 'stroke_color': colors['entity_color']}
lines += u"""\n		<rect x="%(x)s" y="%(y)s" width="116" height="112" fill="%(color)s" stroke="%(stroke_color)s" stroke-width="2"/>""" % {'x': -58+x, 'y': -56+y, 'color': colors['transparent_color'], 'stroke_color': colors['entity_stroke_color']}
lines += u"""\n		<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -58+x, 'y0': -32+y, 'x1': 58+x, 'y1': -32+y, 'stroke_color': colors['entity_stroke_color']}
lines += u"""\n	</g>""" % {}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">MEMBRE</text>""" % {'x': -26+x, 'y': -39.1+y, 'text_color': colors['entity_cartouche_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">idMembre</text>""" % {'x': -53+x, 'y': -15.1+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n	<line x1="%(x0)s" y1="%(y0)s" x2="%(x1)s" y2="%(y1)s" stroke="%(stroke_color)s" stroke-width="1"/>""" % {'x0': -53+x, 'y0': -13+y, 'x1': 7+x, 'y1': -13+y, 'stroke_color': colors['entity_attribute_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">pseudoMembre</text>""" % {'x': -53+x, 'y': 0.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">mailMembre</text>""" % {'x': -53+x, 'y': 16.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">passwordMembre</text>""" % {'x': -53+x, 'y': 32.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n	<text x="%(x)s" y="%(y)s" fill="%(text_color)s" font-family="Verdana" font-size="12">rangMembre</text>""" % {'x': -53+x, 'y': 48.9+y, 'text_color': colors['entity_attribute_text_color']}
lines += u"""\n</g>""" % {}
lines += u'\n</svg>'

import codecs
codecs.open("Membres.svg","w","utf8").write(lines)
safe_print_for_PHP(u'Fichier de sortie "Membres.svg" généré avec succès.')