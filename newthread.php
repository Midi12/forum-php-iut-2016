<?php
	if(!isset($_SESSION))
		session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>FORUM IUT PHP 2016</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
	<body>
		<!-- header -->
		<?php 
			require_once("header.php");
			if (!isset($_SESSION['username']))
				header('Location: index.php');
		?>
		
		<!-- insert nav here -->
		
		<!-- header -->
		
		<!-- content -->
		<!-- CONTENT INSIDE DIVCONTENT WILL BE DYNAMICALY GENERATED -->
		
		<div id="divcontent">
			<form name="postthreadform" action="postthread.php" method="POST">
				<label for="title">Title: </label><input name="title" type="title"><br>
				<label for="content">Subject: </label><br>
				<textarea name="content" rows="6" cols="70"></textarea><br>
				<div class="divinline"><label for="tags">Tags: </label><input name="tags" type="text" onkeyup="OnTagInputKeyUp()"></div><br>
				<span class="note">Tags should be separated by a semi-colon and must not start with a spacer (#)</span><br>
				<input type="submit" value="Post reply">
			</form>
		</div>
		<!-- content -->
		
		<!-- footer -->
		<?php 
			require_once("footer.php");
		?>
		<!-- footer -->
		
		<script src="scripts/jquery.js"></script>
		<script src="scripts/scripts.js"></script>
	</body>
</html>