<?php
	if(!isset($_SESSION))
		session_start();
?>

<?php
	//session active check
	if(session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['username']))
	{
		//sanity checks
		if (!isset($_POST['content']) || !isset($_POST['tid']) || !isset($_POST['mid']))
		{
			header("location:index.php");
		}
		else
		{
			require_once("db_connection.php");
			$bdd = connect_db();
			$query = "UPDATE message SET contenumessage = \"" . $_POST['content'] . "\", datemessage = NOW() WHERE message.idsujet = " . $_POST['tid'] . " AND message.idmessage = " . $_POST['mid'];
			
			$q = $bdd->query($query);
			
			if (!$q)
			{
				echo "Erreur: " . $query;
			}
			else
			{
				$query = 'UPDATE sujet SET datemajsujet = NOW() where sujet.idsujet='.$_POST['tid'];
				if (!($q = $bdd->query($query)))
				{
					echo "erreur mysql : " . $query;
				}
				else
				{
					header("location: " . $_SERVER['HTTP_REFERER']);
				}
			}
		}
	}
	else
	{
		header("location:index.php");
	}
?>
