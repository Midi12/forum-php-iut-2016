<?php
	if(!isset($_SESSION))
		session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>FORUM IUT PHP 2016</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
	<body>
		<!-- header -->
		<?php 
			require_once("header.php");
		?>
		
		<!-- insert nav here -->
		
		<!-- header -->
		
		<!-- content -->
		<!-- CONTENT INSIDE DIVCONTENT WILL BE DYNAMICALY GENERATED -->
		<!--<div id="divcontent">
			<div class="member">
				<span>USERNAME</span><br>
				Total posts : 42<br>
				Threads started : 12<br>
				<a href="#" class="default">Find all posts by USERNAME</a><br>
				<a href="#" class="default">Find all threads started by USERNAME</a><br>
			</div>
				
		</div>-->
		<div id="divcontent">
			<div class="member">
				<?php
					if(!isset($_GET['username']))
					{
						echo "<span>Unknown member ...</span>";
					}
					else
					{
						echo "<span>" . $_GET['username'] . "</span><br>";
						
						require_once("db_connection.php");
						$bdd = connect_db();
						
						if ($q = $bdd->query("SELECT COUNT(idmessage) FROM message, membre WHERE message.idmembre = membre.idmembre AND membre.pseudomembre = \"" . $_GET['username'] . "\""))
						{
							echo "Total posts : " . $q->fetch()[0] . "<br>";	
						}
						if ($q = $bdd->query("SELECT COUNT(idsujet) FROM sujet, membre WHERE sujet.idmembre = membre.idmembre AND membre.pseudomembre = \"" . $_GET['username'] . "\""))
						{
							echo "Threads started : " . $q->fetch()[0] . "<br>";	
						}
						
						echo "<a href=\"showposts.php?username=" . $_GET['username'] . "\" class=\"default\">Find all posts by " . $_GET['username'] . "</a><br>";
						echo "<a href=\"showthreads.php?username=" . $_GET['username'] . "\" class=\"default\">Find all threads started by " . $_GET['username'] . "</a><br>";
					}
				?>
			</div>	
		</div>
		<!-- content -->
		
		<!-- footer -->
		<?php 
			require_once("footer.php");
		?>
		<!-- footer -->
	</body>
</html>