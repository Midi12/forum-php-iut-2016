<?php
	if(!isset($_SESSION))
		session_start();
?>

<?php
	//session active check
	if(session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['username']))
	{
		//sanity checks
		if (!isset($_POST['tag']))
		{
			echo "$tag";
			//header("location:index.php");
		}
		else
		{
			require_once("db_connection.php");
			$bdd = connect_db();
			$query = "SELECT nomcat FROM categorie WHERE nomcat LIKE '" . $_POST['tag'] . "%'";
			
			$q = $bdd->query($query);
			
			if (!$q)
			{
				// erreur à retourner
				$resp['status'] = 0;
				$resp['data'] = array();
				$resp['error'] = $query;
			}
			else
			{
				// succès à retourner
				$resp['status'] = 1;
				$resp['data'] = array();
				
				$i = 0;
				while($n = $q->fetch())
				{
					$resp['data'][$i] = $n['nomcat'];
					$i++;	
				}
			}
			
			echo json_encode($resp);
			exit();
		}
	}
	else
		echo "unknown session";
?>