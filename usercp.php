<?php
	if(!isset($_SESSION))
		session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>FORUM IUT PHP 2016</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
	<body>
		<!-- header -->
		<?php 
			require_once("header.php");
		?>
		
		<!-- insert nav here -->
		
		<!-- header -->
		
		<!-- content -->
		<!-- CONTENT INSIDE DIVCONTENT WILL BE DYNAMICALY GENERATED -->
		<!--<div id="divcontent">
			<div class="member">
				<span>USERNAME</span><br>
				Total posts : 42<br>
				Threads started : 12<br>
				<a href="#" class="default">Find all posts by USERNAME</a><br>
				<a href="#" class="default">Find all threads started by USERNAME</a><br>
			</div>
				
		</div>-->
		<br>
		<div id="divcontent">
			<div class="member">
				<?php
					if(!isset($_GET['username']))
					{
						echo "<span>Unknown member ...</span>";
					}
					else
					{
						echo "<br><span><a class=\"member\" href=\"member.php?username=" . $_GET['username'] . "\">" . $_GET['username'] . "</a></span><br><br><br>";
						echo "Edit your details :";
						?>
						<form id="formmod" name="formreg" method="POST" action="modifyuser.php">
							<input id="usernamebox" name="usernamebox" type="reg" placeholder="Username" pattern="[0-9A-Za-z\-]{2,}">
							<div class="inputvalidation"></div><br>
							<input id="emailbox" name="emailbox" type="email" placeholder="Mail" pattern="[0-9a-z-_+.]+@[0-9a-z-_+]+.[a-z]{2,4}">
							<div class="inputvalidation"></div><br>
							<input id="passwordbox" name="passwordbox" type="password" placeholder="Password" pattern="[0-9A-Za-z\-]{5,}">
							<div class="inputvalidation"></div><br>
							<input id="submitlogin" name="submitreg" type="submit" value="Update">
						</form>
						<?php
					}
				?>
			</div>	
		</div>
		<!-- content -->
		
		<!-- footer -->
		<?php 
			require_once("footer.php");
		?>
		<!-- footer -->
	</body>
</html>
