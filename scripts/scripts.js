function ShowReplyForm()
{
	$("div.divreply").html("<form name=\"postmessageform\" action=\"postmessage.php\" method=\"POST\"><input type=\"hidden\" name=\"tid\" value=\""
		+ decodeURIComponent(window.location.search.match(/(\?|&)id\=([^&]*)/)[2]) 
		+ "\"><textarea name=\"content\" rows=\"6\" cols=\"70\"></textarea><br><input type=\"submit\" value=\"Post reply\"></form>");
}

function ShowEditForm(id)
{
	$("a.button[id=" + id + "]").parent().html("<form name=\"editmessageform\" action=\"editmessage.php\" method=\"POST\"><input type=\"hidden\" name=\"tid\" value=\""
		+ decodeURIComponent(window.location.search.match(/(\?|&)id\=([^&]*)/)[2]) 
		+ "\"><input type=\"hidden\" name=\"mid\" value=\"" + id + "\"><textarea name=\"content\" rows=\"6\" cols=\"70\"></textarea><br><input type=\"submit\" value=\"Edit message\"></form>");
}

function strIsBlank(str)
{
    return (!str || /^\s*$/.test(str));
}

function strIsEmpty(str)
{
    return (!str || 0 === str.length);
}

function OnTagInputKeyUp()
{
	var input_obj = $("input[name=tags]").get(0);
	
	// sanitize input
	var last_char = input_obj.value[input_obj.value.length - 1];
	if (last_char == "#" || last_char == " " || last_char == "\"" || last_char == "'")
	{
		input_obj.value = input_obj.value.substring(0, input_obj.value.length - 1);
		return;
	}
	
	var tag_array = input_obj.value.split(";");
	
	// sanitize array
	var i = 0;
	for (i = 0; i < tag_array.length; i++)
	{
		var tag = tag_array[i];
		
		if (strIsEmpty(tag) 
		|| strIsBlank(tag) 
		|| tag.contains("#") 
		|| tag.contains(" ") 
		|| tag.contains("'") 
		|| tag.contains("\""))
		{
			tag_array.splice(i, 1);
		}
	}
	
	$(".listbox").remove();
	
	if(tag_array.length > 0)
	{
		var listhtml = "<ul class=\"listbox\">";
		
		var result = null;
		var tags = [];
		
		$.ajaxSetup({async: false});
		
		$.post("searchtag.php", { tag: tag_array[tag_array.length - 1] },
			function (data)
			{
				console.log(data);
				console.log(data.status);
				console.log(data.data);
				
				if (data.status == 1)
				{
					result = data.status;
					tags = JSON.parse(JSON.stringify(data.data));
					
					var k = 0;
					for (k = 0; k < tags.length; k++)
						//console.log(tags[k]);
						listhtml += "<li class=\"listboxitem\"><a class=\"tag\" value=\"" + tags[k] + "\" onclick=\"OnProposedTagClick(this)\">#" + tags[k] + "</a></li>"
				}
				else
				{
					console.log(data.error);
					alert(data.error);
				}
				
			}, "json");
		
		$.ajaxSetup({async: true});
		
		listhtml += "</ul>";
		
		$(listhtml).insertAfter("input[name=tags]");
	}	
}

function OnProposedTagClick(item)
{
	var input_obj = $("input[name=tags]").get(0);
	var tag_array = input_obj.value.split(";");
	tag_array[tag_array.length - 1] = $(item).attr("value");
	
	var str = "";
	var i = 0;
	for (i = 0; i < tag_array.length; i++)
		str += tag_array[i] + ";";
		
	input_obj.value = str;
	$(".listbox").remove();
}

function confirmDeleteThread(id)
{
	if (confirm('Do you want to delete this thread?'))
	{
		window.location.replace("delete.php?thread=" + id);
	}
}

function confirmDeletePost(id)
{
	if (confirm('Do you want to delete this post?'))
	{
		window.location.replace("delete.php?post=" + id);
	}
}