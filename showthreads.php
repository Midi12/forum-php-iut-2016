<?php
	if(!isset($_SESSION))
		session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>FORUM IUT PHP 2016</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
	<body>
		<!-- header -->
		<?php 
			require_once("header.php");
		?>
		
		<!-- insert nav here -->
		
		<!-- header -->
		
		<!-- content -->
		<!-- CONTENT INSIDE DIVCONTENT WILL BE DYNAMICALY GENERATED -->
		<div id="divcontent">
			<?php
				if(!isset($_GET['username']))
				{
					echo "<span>Unknown member ...</span>";
				}
				else
				{
					require_once("db_connection.php");
					$bdd = connect_db();
					
					if($q = $bdd->query("SELECT * FROM sujet, membre WHERE sujet.idmembre = membre.idmembre AND pseudomembre = \"" . $_GET['username'] .  "\""))
					{
						while($thread = $q->fetch())
						{
							/*if($q2 = $bdd->query("SELECT * FROM sujet WHERE idsujet = " . $msg['idsujet']))
							{
								$sujet = $q2->fetch()['titresujet'];
								echo "<div class=\"answer dynorder\">";
									echo "<span class=\"note\">submission date " . $msg['datemessage'] . "</span><br>";
									echo "Thread title : " . $sujet . "<br>";
									echo "by <a href=\"#\" class=\"member\">" . $_GET['username'] . "</a><br>";
									echo "<p class=\"message\">" . $msg['contenumessage'] . "<p>";
								echo "</div>";
							}*/
							
							echo "<div class=\"subject\">";
							echo "<a href=\"thread.php?id=" . $thread['idsujet'] . "\" class=\"title\">" . $thread['titresujet'] . "</a><br>";
							echo "<span class='note'>Submission Date: ".$thread['datesujet']."</span><br/>";
							require_once('utils.php'); displaytag($thread['idsujet']);
							echo "</div>";
						}
					}
				}
			?>
		</div>
		<!-- content -->
		
		<!-- footer -->
		<?php 
			require_once("footer.php");
		?>
		<!-- footer -->
	</body>
</html>
