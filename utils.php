<?php
function displaytag($id)
{
	require_once("db_connection.php");
	$bdd = connect_db();
	$cats = $bdd->query('SELECT * FROM appartenir NATURAL JOIN categorie where idsujet = '.$id.' ORDER BY nomCat;');
	while ($cat = $cats->fetch())
	{
		echo '<a href="index.php?tag='.$cat['idcat'].'" class="tag">#'.$cat['nomcat'].'</a>';
	}
}

function messageCount()
{
	require_once("db_connection.php");
	$bdd = connect_db();
	$q = $bdd->query("SELECT COUNT(idsujet) FROM message");
	$d = $q->fetch();
	return $d['COUNT(idsujet)'];
}

function debug_to_console( $data )
{
    if ( is_array( $data ) )
        $output = "<script>console.log( 'Debug : " . implode( ',', $data) . "' );</script>";
    else
        $output = "<script>console.log( 'Debug : " . $data . "' );</script>";

    echo $output;
}
?>
