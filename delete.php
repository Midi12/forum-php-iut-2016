<?php
	if(!isset($_SESSION))
		session_start();
?>
<?php
	if (session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['rank']))
	{
		if ($_SESSION['rank'] == 'ADMIN')
		{
			require_once("db_connection.php");
			$bdd = connect_db();
			if (isset($_GET['thread']))
			{
				if ($qMessage = $bdd->query("DELETE FROM message WHERE idsujet=".$_GET['thread']))
					echo 'delete complete';
				else
					echo 'delete failed';
				if ($qTag = $bdd->query("DELETE FROM appartenir WHERE idsujet=".$_GET['thread']))
					echo 'delete complete';
				else
					echo 'delete failed';
				if ($qThread = $bdd->query("DELETE FROM sujet WHERE idsujet=".$_GET['thread']))
					echo 'delete complete';
				else
					echo 'delete failed';
			}
			echo 'query failed';
			if (isset($_GET['post']))
			{
				if ($qMessage = $bdd->query("DELETE FROM message WHERE idmessage=".$_GET['post']))
					echo 'delete complete';
				else
					echo 'delete failed';
			}
			echo 'query failed';
		}
		else
			echo 'Not an ADMIN';
	}
	else
		echo 'Session not initialized';
	header('Location: '.$_SERVER['HTTP_REFERER']);
?>
