CREATE TABLE membre (
	idmembre INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	pseudomembre VARCHAR(50) NOT NULL UNIQUE,
	mailmembre VARCHAR(50),
	passwordmembre VARCHAR(10),
	rangmembre VARCHAR(50)
)ENGINE=InnoDB;

CREATE TABLE sujet (
	idsujet INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	titresujet VARCHAR(100),
	idmembre INT UNSIGNED,
	datesujet DATETIME,
	datemajsujet DATETIME,
	FOREIGN KEY (idmembre) REFERENCES membre (idmembre)
)ENGINE=InnoDB;

CREATE TABLE message (
	idsujet INT UNSIGNED,
	idmessage INT UNSIGNED,
	contenumessage VARCHAR(2000),
	idmembre INT UNSIGNED,
	datemessage DATETIME,
	PRIMARY KEY (idsujet, idmessage),
	FOREIGN KEY (idmembre) REFERENCES membre (idmembre),
	FOREIGN KEY (idsujet) REFERENCES sujet (idsujet)
)ENGINE=InnoDB;

CREATE TABLE categorie (
	idcat INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nomcat VARCHAR(50)
)ENGINE=InnoDB;

CREATE TABLE appartenir (
	idcat INT UNSIGNED,
	idsujet INT UNSIGNED,
	PRIMARY KEY (idcat, idsujet),
	FOREIGN KEY (idsujet) REFERENCES sujet (idsujet),
	FOREIGN KEY (idcat) REFERENCES categorie (idcat)
)ENGINE=InnoDB;
