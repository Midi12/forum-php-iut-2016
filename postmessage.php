<?php
	if(!isset($_SESSION))
		session_start();
?>

<?php
	//session active check
	if(session_status() == PHP_SESSION_ACTIVE and isset($_SESSION) and isset($_SESSION['username']))
	{
		//sanity checks
		if (!isset($_POST['content']) || !isset($_POST['tid']))
		{
			header("location:index.php");
		}
		else
		{
			require_once("db_connection.php");
			$bdd = connect_db();
			
			$prequery0 = "SELECT idmembre FROM membre WHERE pseudomembre = '" . $_SESSION['username'] . "'";
			
			if ($q0 = $bdd->query($prequery0))
			{
				$idmembre = $q0->fetch()[0];
			}
			else
			{
				echo "erreur mysql : " . $prequery0;
			}
			
			$prequery1 = "SELECT COUNT(idmessage) FROM message WHERE idsujet = " . $_POST['tid'];
			
			if ($q1 = $bdd->query($prequery1))
			{
				$idnewmessage = $q1->fetch()[0];
				$idnewmessage = intval($idnewmessage);
				$idnewmessage++;
			}
			else
			{
				echo "erreur mysql : " . $prequery1;
			}
			$query = "INSERT INTO message (idsujet, idmessage, contenumessage, idmembre, datemessage) VALUES (" . $_POST['tid'] . ", " . $idnewmessage . ", \"" . $_POST['content'] . "\", " . $idmembre . ", NOW());";
			
			if ($q = $bdd->query($query))
			{
				$query = 'UPDATE sujet SET datemajsujet = NOW() where sujet.idsujet='.$_POST['tid'];
				if (!($q = $bdd->query($query)))
				{
					echo "erreur mysql : " . $query;
				}
				else
				{
					header("location:" . $_SERVER['HTTP_REFERER']);
				}
			}
			else
			{
				echo "erreur mysql : " . $query;
			}
		}
	}
	else
	{
		header("location:index.php");
	}
?>
